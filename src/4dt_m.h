/**
    \file  4dt_conf.h
    \brief Header of the 4D-TRIS math module.
 */

#ifndef _4DT_M_H

#define _4DT_M_H

/*------------------------------------------------------------------------------
   DECLARATIONS
------------------------------------------------------------------------------*/

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795029L
#endif

extern void mSolveSqrEq(double a, double b, double c, double *n1, double *n2);

#endif
